# GeneratePasswordAPI
GeneratePassword is a golang application for generating Random Passwords for your use case 

Currently Supports a password length, injects 4 numerals, injects 2 ascii characters, then randomizes capital alphabetical characters.

# Basic Usage
```go
package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/shellfu/gotower"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)
	p := Password(14, true, true)
	logger.Log("msg", p)

	var svc PasswordService
	svc = passwordService{}

	passwordHandler := httptransport.NewServer(
		makeGeneratePasswordEndpoint(svc),
		decodePasswordRequest,
		encodeResponse,
	)

	http.Handle("/password", passwordHandler)
	logger.Log("msg", "HTTP", "addr", ":8080")
	logger.Log("err", http.ListenAndServe(":8080", nil))
}

```

## Curl to API for Password Generation
### Payload:
##### l should be an integer for length of the password.
##### a is a boolean if you want to include ascii chars in the password.
##### n is a boolean if you want to include numerals in the password.

```bash
curl -X POST 'http://localhost:8080/password' -d '{"l":14,"a":true,"n":true}'

```

