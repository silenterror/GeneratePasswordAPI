package genpass

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-kit/kit/endpoint"
)

// MakeGeneratePasswordEndpoint - endpoint for generate password
func MakeGeneratePasswordEndpoint(svc PasswordService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(passwordRequest)
		log.Printf("REQUEST: %v\n", req)
		v, _ := svc.GeneratePassword(req.Passlen, req.Passascii, req.Passints)
		log.Printf("RESPONSE: %v\n", passwordResponse{v, ""})
		return passwordResponse{v, ""}, nil
	}
}

func DecodePasswordRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request passwordRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	log.Printf("encoding: %v\n", response)
	return json.NewEncoder(w).Encode(response)
}

type passwordRequest struct {
	Passlen   int  `json:"l"`
	Passascii bool `json:"a"`
	Passints  bool `json:"n"`
}

type passwordResponse struct {
	V   string `json:"v"`
	Err string `json:"err,omitempty"`
}
