package genpass

import (
	"time"

	"github.com/go-kit/kit/log"
)

type LoggingMiddleware struct {
	Logger log.Logger
	Next   PasswordService
}

func (mw LoggingMiddleware) GeneratePassword(length int, ascii bool, num bool) (s string, e error) {
	defer func(begin time.Time) {
		_ = mw.Logger.Log(
			"method", "GeneratePassword",
			"length", length,
			"ascii", ascii,
			"num", num,
			"return", s,
			"took", time.Since(begin),
		)
	}(time.Now())

	s, _ = mw.Next.GeneratePassword(length, ascii, num)
	return
}
