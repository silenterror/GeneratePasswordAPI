package genpass

import (
	"errors"
	"log"
	"math/rand"
	"strings"
	"time"
)

// PasswordService - service for password operations
type PasswordService interface {
	GeneratePassword(length int, ascii bool, num bool) (string, error)
}

// PasswordSvc - type for the service
type PasswordSvc struct{}

// RandomInt - generates a random number in a range given
func RandomInt(min int, max int) (n int) {
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)
	result := r.Intn(max-min) + min
	return result
}

// Alphabetchars - Generates a []string of random alphabetical characters
func Alphabetchars(count int) []string {
	var countSet []string
	chars := "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
	onechar := strings.Split(chars, ",")

	for i := 0; i < count; i++ {
		r := RandomInt(0, 26)
		a := onechar[r]
		countSet = append(countSet, a)
	}
	return countSet
}

// InjectASCII - Adds ascii characters to the []string
func InjectASCII(target []string, count int) []string {
	chars := "~a!a@a#a$a%a^a&a*a(a)a-a_a+a=a{a}a[a]a\\a|a<a,a>a.a?a/a\"a'a;a:a`a"
	onechar := strings.Split(chars, "a")

	for i := 0; i < count; i++ {
		r := RandomInt(0, 31)
		a := onechar[r]
		target = append(target, a)
	}
	return target
}

// InjectNumerals - Adds integers to the []string
func InjectNumerals(target []string, count int) []string {
	ints := "1:2:3:4:5:6:7:8:9:0"
	nums := strings.Split(ints, ":")
	for i := 0; i < count; i++ {
		r := RandomInt(0, 9)
		a := nums[r]
		target = append(target, a)
	}
	return target
}

// Shuffle the []string
func Shuffle(src []string) []string {
	result := make([]string, len(src))
	rand.Seed(time.Now().UTC().UnixNano())
	perm := rand.Perm(len(src))

	for i, v := range perm {
		result[v] = src[i]
	}
	return result
}

// RandomCapitalize - Randomly capitalized letters in a []string
func RandomCapitalize(target []string) []string {
	for i, v := range target {
		rint := RandomInt(1, 3)
		if rint%2 == 0 {
			target[i] = strings.ToUpper(v)
		}

	}
	return target
}

// GeneratePassword - generates a password string
func (p PasswordSvc) GeneratePassword(length int, ascii bool, num bool) (string, error) {
	var lenErr = errors.New("Length of Password Must be at least 7. Ex: \"l\":\"7\"")

	var alphalen int
	if length < 7 {
		log.Print(lenErr)
		return "", lenErr
	}
	// for simplicity it grants the password 2 ascii characters
	if ascii == true {
		alphalen = length - 2
	}

	// for simplicity it grants the password 4 numbers
	if num == true {
		alphalen = alphalen - 4
	}

	a := Alphabetchars(alphalen)
	b := RandomCapitalize(a)

	ai := InjectASCII(b, 2)
	ai = InjectNumerals(ai, 4)

	ai = Shuffle(ai)
	return strings.Join(ai, ""), nil

}

// Password - generates a password string
func Password(length int, ascii bool, num bool) string {
	var alphalen int
	// for simplicity it grants the password 2 ascii characters
	if ascii == true {
		alphalen = length - 2
	}

	// for simplicity it grants the password 4 numbers
	if num == true {
		alphalen = alphalen - 4
	}

	a := Alphabetchars(alphalen)
	b := RandomCapitalize(a)

	ai := InjectASCII(b, 2)
	ai = InjectNumerals(ai, 4)

	ai = Shuffle(ai)
	return strings.Join(ai, "")

}
