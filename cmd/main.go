package main

import (
	"net/http"
	"os"

	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	"gitlab.com/silenterror/genpassapi/pkg/genpass"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)
	p := genpass.Password(14, true, true)
	logger.Log("msg", p)

	var svc genpass.PasswordService
	svc = genpass.PasswordSvc{}
	svc = genpass.LoggingMiddleware{
		Logger: logger,
		Next:   svc,
	}

	passwordHandler := kithttp.NewServer(
		genpass.MakeGeneratePasswordEndpoint(svc),
		genpass.DecodePasswordRequest,
		genpass.EncodeResponse,
	)

	http.Handle("/password", passwordHandler)
	logger.Log("msg", "HTTP", "addr", ":8080")
	logger.Log("err", http.ListenAndServe(":8080", nil))
}
